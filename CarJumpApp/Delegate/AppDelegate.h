//
//  AppDelegate.h
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContextForTesting;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinatorForTesting;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

