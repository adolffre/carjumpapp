//
//  MapViewController.h
//  CarJumpApp
//
//  Created by A. J. on 02/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <GoogleMaps/GoogleMaps.h>
#import "DBManager.h"
#import "MarkerDBM.h"
#import "Marker.h"

@interface MapViewController : UIViewController<GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) DBManager *dbm;
@property (nonatomic, strong) MarkerDBM *markerDBM;
@property (nonatomic, strong) NSMutableArray *allMarkers;
@property (nonatomic, strong) NSMutableArray *discartedFromAllMarkers;
@end