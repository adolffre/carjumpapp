//
//  MapViewController.m
//  CarJumpApp
//
//  Created by A. J. on 02/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "MapViewController.h"

@implementation MapViewController

#pragma mark - ViewCycleLife
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeProperties];
    
}
#pragma mark - Initializer
-(void)initializeProperties{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:+52.5
                                                            longitude:+13.4
                                                                 zoom:10];
    self.mapView.camera = camera;
    self.dbm = [DBManager sharedManager];
    self.markerDBM = [[MarkerDBM alloc] initWithDBM:self.dbm];
    self.discartedFromAllMarkers = [NSMutableArray new];
    
    self.allMarkers = [NSMutableArray arrayWithArray:[self.markerDBM allMarkers]];
    
}
#pragma mark - GMSMapviewDelegate
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        for (GMSAddress *addressObj in [response results]) {
            if(addressObj.locality && addressObj.country){
                
                Marker *m = [self saveMarker:addressObj];
                [self.allMarkers addObject:m];
                [self putMarkerOnMap:m];
                break;
            }
        }
    }];
}
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    GMSVisibleRegion region = self.mapView.projection.visibleRegion;
    
    for (Marker *m in self.allMarkers) {
        
        if([self isMarker:m inVisibleRegion:region]){
            [self putMarkerOnMap:m];
        }
    }
    [self clearMarkerArray];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    if(self.mapView.selectedMarker){
        self.mapView.selectedMarker = nil;
    }
    [self performSelector:@selector(hideMarker:) withObject:marker afterDelay:3.0];
    return NO;
}
-(void)hideMarker:(GMSMarker *)marker {
    if(marker == self.mapView.selectedMarker)
        self.mapView.selectedMarker = nil;
}
#pragma mark - Custom Methods
-(void)putMarkerOnMap:(Marker *)marker{
    CLLocationCoordinate2D cord = CLLocationCoordinate2DMake([marker.lat doubleValue], [marker.longi doubleValue]);
    GMSMarker *gmMarker = [GMSMarker markerWithPosition:cord];
    gmMarker.title = [NSString stringWithFormat:@"%@ - %@",marker.city,marker.country];
    gmMarker.map = self.mapView;
    gmMarker.icon = [UIImage imageNamed:@"Pin_Blue"];
    gmMarker.groundAnchor = CGPointMake(0.18, 0.9);
    gmMarker.appearAnimation = kGMSMarkerAnimationPop;

    [self.discartedFromAllMarkers addObject:marker];
    
}
-(Marker *)saveMarker:(GMSAddress *)gmAddress{
    
    Marker *mark = [self.markerDBM newMarkerTx];
    mark.city = gmAddress.locality;
    mark.country = gmAddress.country;
    mark.lat = [NSNumber numberWithDouble:(double)gmAddress.coordinate.latitude];
    mark.longi = [NSNumber numberWithDouble:(double)gmAddress.coordinate.longitude];
    
    NSError *error = [self.markerDBM commit];
    if(error){
        [self showMessageError:error withText:@"Error while saving location!"];
        return nil;
    }
    
    return mark;
    
}

-(BOOL)isMarker:(Marker *)marker inVisibleRegion:(GMSVisibleRegion)region
{
    GMSCoordinateBounds * bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    CLLocationCoordinate2D cord = CLLocationCoordinate2DMake([marker.lat doubleValue], [marker.longi doubleValue]);
    return [bounds containsCoordinate:cord];
}

-(void)clearMarkerArray{
    [self.allMarkers removeObjectsInArray:self.discartedFromAllMarkers];
    [self.discartedFromAllMarkers removeAllObjects];
}
#pragma mark - UIAlertController
-(void)showMessageError:(NSError *)error withText:(NSString *)text{
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Error!"
                                                                        message: text? text : [NSString stringWithFormat:@"%@",error]
                                                                 preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Ok"
                                                          style: UIAlertActionStyleDestructive
                                                        handler: nil];
    
    [controller addAction: alertAction];
    
    [self presentViewController: controller animated: YES completion: nil];
    
    
}
@end
