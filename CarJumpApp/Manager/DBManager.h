//
//  DBManager.h
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DBManager : NSObject


#pragma mark - Singleton

+ (instancetype)sharedManager;
+ (instancetype)sharedManagerForTesting;

#pragma mark - Properties

@property (nonatomic) NSManagedObjectContext *context;

#pragma mark - Methods


- (NSError *)saveContext;

- (NSFetchRequest *)newFetchRequestForEntity:(NSString *)entityName ;

- (NSArray *)executeFetchRequest:(NSFetchRequest *)fetchRequest;

- (BOOL)removeAllRecordsFromEntity:(NSString *)entityName;
- (id)allObjectsForEntity:(NSString *)entityName
             andPredicate:(NSPredicate *)predicate;
@end
