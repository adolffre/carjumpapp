//
//  DBManager.m
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "DBManager.h"
#import "AppDelegate.h"

@implementation DBManager

#pragma mark - Singleton Methods

+ (instancetype)sharedManager {
    static DBManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    });
    return sharedMyManager;
}

+ (instancetype)sharedManagerForTesting {
    static DBManager *sharedManagerForTesting = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManagerForTesting = [[self alloc] init];
        sharedManagerForTesting.context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContextForTesting];
    });
    return sharedManagerForTesting;
}

#pragma mark - Core Data

- (NSError *)saveContext {
    NSError *error;
    if (![self.context save:&error]) {
        return error;
    }
    return nil;
}

- (NSFetchRequest *)newFetchRequestForEntity:(NSString *)entityName {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    return fetchRequest;
}

- (NSArray *)executeFetchRequest:(NSFetchRequest *)fetchRequest {
    NSError *error;
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        return nil;
    } else {
        return fetchedObjects;
    }
}

- (id)allObjectsForEntity:(NSString *)entityName
             andPredicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [self newFetchRequestForEntity:entityName];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    return [self executeFetchRequest:fetchRequest];
}


- (BOOL)removeAllRecordsFromEntity:(NSString *)entityName {
    NSArray *allRecords = [self allObjectsForEntity:entityName
                                       andPredicate:nil];
    if (allRecords) {
        for (NSManagedObject *record in allRecords) {
            [self.context deleteObject:record];
        }
    }
    return YES;
}



@end
