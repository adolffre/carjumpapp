//
//  MarkerDBM.h
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"
#import "Marker.h"

@interface MarkerDBM : NSObject
#pragma mark - Properties
@property(nonatomic) DBManager *dbm;
#pragma mark - Initializer
-(id)initWithDBM:(DBManager *)dbm;
#pragma mark - Read
- (NSArray *)allMarkers;
#pragma mark - Create
- (Marker *)newMarkerTx;
#pragma mark - Update
- (NSError *)commit;
#pragma mark - Delete
- (BOOL)removeRecord:(Marker *)record;
@end
