//
//  MarkerDBM.m
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import "MarkerDBM.h"

@implementation MarkerDBM

#pragma mark - Initializer
-(id)initWithDBM:(DBManager *)dbm{
    self = [super init];
    if (self != nil) {
        self.dbm = dbm;
    }
    return self;
}

#pragma mark - Read

- (NSArray *)allMarkers {
    NSFetchRequest *fetchRequest = [self.dbm newFetchRequestForEntity:@"Marker"];
    return [self.dbm executeFetchRequest:fetchRequest];
}

#pragma mark - Create

- (Marker *)newMarkerTx{
    Marker *record = (Marker *)[NSEntityDescription insertNewObjectForEntityForName:@"Marker" inManagedObjectContext:self.dbm.context];
    return record;
}

#pragma mark - Update

- (NSError *)commit {
    return [self.dbm saveContext];
}

#pragma mark - Delete

- (BOOL)removeRecord:(Marker *)record {
    [self.dbm.context deleteObject:record];
    return YES;
}



@end
