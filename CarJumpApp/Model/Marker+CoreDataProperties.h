//
//  Marker+CoreDataProperties.h
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Marker.h"

NS_ASSUME_NONNULL_BEGIN

@interface Marker (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *lat;
@property (nullable, nonatomic, retain) NSNumber *longi;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *country;

@end

NS_ASSUME_NONNULL_END
