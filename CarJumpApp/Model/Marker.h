//
//  Marker.h
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Marker : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Marker+CoreDataProperties.h"
