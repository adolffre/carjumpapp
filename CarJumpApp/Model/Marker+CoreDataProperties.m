//
//  Marker+CoreDataProperties.m
//  CarJumpApp
//
//  Created by A. J. on 01/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Marker+CoreDataProperties.h"

@implementation Marker (CoreDataProperties)

@dynamic lat;
@dynamic longi;
@dynamic city;
@dynamic country;

@end
