//
//  DBManagerTest.m
//  CarJumpApp
//
//  Created by A. J. on 02/10/15.
//  Copyright © 2015 AJ. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DBManager.h"
#import "Marker.h"
@interface DBManagerTest : XCTestCase
@property (nonatomic) DBManager *dmb;
@end

@implementation DBManagerTest

- (void)setUp {
    [super setUp];
     self.dmb = [DBManager sharedManagerForTesting];
    [self.dmb removeAllRecordsFromEntity:@"Marker"];
}

#pragma mark - Core Data

- (void)testContext {
    XCTAssertNotNil([self.dmb context]);
}

- (void)testNewFetchRequest {
    XCTAssertNotNil([self.dmb newFetchRequestForEntity:@"Marker"]);
}

- (void)testExecuteFetchRequest {
    NSFetchRequest *fetchRequest = [self.dmb newFetchRequestForEntity:@"Marker"];
    XCTAssertNotNil([self.dmb executeFetchRequest:fetchRequest]);
}
- (void)testSaveContext {
    Marker *record = (Marker *) [NSEntityDescription insertNewObjectForEntityForName:@"Marker" inManagedObjectContext:self.dmb.context];
    record.city = @"city1";
    
    NSError *error = [self.dmb saveContext];
    XCTAssertNil(error);
    
}
- (void)testRemoveAllRecordsFromEntity{
    Marker *record = (Marker *) [NSEntityDescription insertNewObjectForEntityForName:@"Marker" inManagedObjectContext:self.dmb.context];
    record.city = @"city1";
    
    [self.dmb saveContext];
    XCTAssertTrue([self.dmb removeAllRecordsFromEntity:@"Marker"]);
}
- (void)testAllObjectsForEntity{
    Marker *record = (Marker *) [NSEntityDescription insertNewObjectForEntityForName:@"Marker" inManagedObjectContext:self.dmb.context];
    record.city = @"city1";
    
    [self.dmb saveContext];

    NSArray *all = [self.dmb allObjectsForEntity:@"Marker" andPredicate:nil];
    
    XCTAssertNotNil(all);
    XCTAssertTrue(all.count == 1);
}




@end
